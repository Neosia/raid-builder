# Raid Builder

## Requirements
- Install Node.js
- Install Angular/CLI : `npm install -g @angular/cli`
- Install Nodemon : `npm install -g nodemon`

## Run project

### For node server
`cd backend`  
`npm install`  
`nodemon server`

### For angular frontend
`cd frontend`  
`npm install`  
`ng serve`


